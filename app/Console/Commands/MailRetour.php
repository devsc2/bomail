<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Classes\Wrapper;

class MailRetour extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:retour';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoie un mail de retour client (avis verifies) au clients donc le voyage c est termine il y a 14 jours';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     public function handle()
     {
       $Today = date('Y-m-d');
       $rqtDate = date("Y-m-d", mktime(0,0,0,date("m"), date("d")-2, date("Y")));

       $blacklistMailRetour = \DB::table('blacklist_mail_retour')->get();
       $blacklistedMails = array_map(function($blacklistedMail){ return $blacklistedMail->id_dossier; }, $blacklistMailRetour);

       $Dossiers = \DB::table('dossiers')
         ->where('date_fin', $rqtDate)
         ->whereNotIn('id', $blacklistedMails)
         ->get();

       foreach ($Dossiers as $dossier) {
         $client = \DB::table('clients')
           ->where('id', $dossier->id_client)
           ->first();

         $lienVendeur = \DB::table('lien_dossier_vendeur')
           ->where('id_dossier', $dossier->id)
           ->first();

         $vendeur = \DB::table('vendeurs')
           ->where('id', $lienVendeur->id_vendeur)
           ->first();

         $Data = [
           'id_dossier' => $dossier->id,
           'id_pkg' => $dossier->id_pkg,
           'client_mail' => $client->mail,
           'civilite' => $client->civilite,
           'client_nom' => $client->nom,
           'client_prenom' => $client->prenom,
           'date_confirm' => $dossier->date_confirm,
           'mailVendeur' => $vendeur->mail,
           'VendeurNom' => $vendeur->nom,
           'VendeurPrenom' => $vendeur->prenom,
           'to' => $dossier->to,
         ];

         //$Data['VendeurPrenom'].' '.$Data['VendeurNom']   Nom et Prenom Vendeur pour nom de l'expediteur

         \Mail::send('email.index2', $Data, function($message) use ($Data){
           $message->to($Data['client_mail'], $Data['civilite'].' '.$Data['client_nom'].' '.$Data['client_prenom'])->subject("Retour Client Voyaneo dossier N°".$Data['id_dossier'])->setFrom([$Data['mailVendeur'] => 'Voyaneo retour Client']);
         });

         \Mail::send('email.index2', $Data, function($message) use ($Data){
           $message->to('fabien@lead-factory.net', 'Fabien')->subject("Retour Client Voyaneo dossier N°".$Data['id_dossier'])->setFrom([$Data['mailVendeur'] => 'Voyaneo retour Client']);
         });

         \Mail::send('email.index2', $Data, function($message) use ($Data){
           $message->to('fabienp@sc2consulting.fr', 'Fabien')->subject("Retour Client Voyaneo dossier N°".$Data['id_dossier'])->setFrom([$Data['mailVendeur'] => 'Voyaneo retour Client']);
         });
     }
   }
}
