<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');


Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function(){
        $user = Auth::user();
        return view("welcome")
            ->with("user", $user);
    });
    Route::get('/test', function () {
        dd(Config::get('mail'));
    });

    Route::get('/mail_retour_test', function () {

        Mail::send('email.index', [], function ($message) {
            $message->to('fabienp@sc2consulting.fr', 'Fabien')->subject('Retour Client Voyaneo')->setFrom(['fabienp@sc2consulting.fr' => 'Fabien PERREON']);
        });
    });

    Route::get('/mail_rqt_test', 'EmailController@rqtDossiers');

    Route::get('/mail_retour_test_Controller/{slugpage}', 'EmailController@sendMail');

    Route::get('/sendingmaileditor/{dossier}', 'DossierController@sendingMailEditor');

    Route::post("/upload_file_{dossier}", 'DossierController@upload_file');
    Route::get("/delete_file_{dossier}_{file}", "DossierController@delete_file");


    Route::post("/send_email_{dossier}", "EmailController@sendMailFactureConfirmationCGV");

    Route::get('/dossiers', "DossierController@showDossiers");
});