<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;


class EmailController extends controller
{

  function rqtDossiers()
  {
    $Today = date('Y-m-d');
    $rqtDate = date("Y-m-d", mktime(0,0,0,date("m"), date("d")-2, date("Y")));
    $rqtDateTest= date("2019-04-14");

    $blacklistMailRetour = \DB::table('blacklist_mail_retour')->get();
    $blacklistedMails = array_map(function($blacklistedMail){ return $blacklistedMail->id_dossier; }, $blacklistMailRetour);

    $Dossiers = \DB::table('dossiers')
      ->where('date_fin', $rqtDate)
      ->whereNotIn('id', $blacklistedMails)
      ->get();

      var_dump($Dossiers);

    foreach ($Dossiers as $dossier) {
      $client = \DB::table('clients')
        ->where('id', $dossier->id_client)
        ->first();

      $lienVendeur = \DB::table('lien_dossier_vendeur')
        ->where('id_dossier', $dossier->id)
        ->first();

      $vendeur = \DB::table('vendeurs')
        ->where('id', $lienVendeur->id_vendeur)
        ->first();

      $Data = [
        'id_dossier' => $dossier->id,
        'id_pkg' => $dossier->id_pkg,
        'client_mail' => $client->mail,
        'civilite' => $client->civilite,
        'client_nom' => $client->nom,
        'client_prenom' => $client->prenom,
        'date_confirm' => $dossier->date_confirm,
        'mailVendeur' => $vendeur->mail,
        'VendeurNom' => $vendeur->nom,
        'VendeurPrenom' => $vendeur->prenom,
        'to' => $dossier->to,
      ];

      //$Data['VendeurPrenom'].' '.$Data['VendeurNom']   Nom et Prenom Vendeur pour nom de l'expediteur
      \Mail::send('email.index2', $Data, function($message) use ($Data){
        $message->to('f.perreon@gmail.com', 'Fabien')->subject("Retour Client Voyaneo dossier N°".$Data['id_dossier'])->setFrom([$Data['mailVendeur'] => 'Voyaneo retour Client']);
      });

      \Mail::send('email.index2', $Data, function($message) use ($Data){
        $message->to('fabienp@sc2consulting.fr', 'Fabien')->subject("Retour Client Voyaneo dossier N°".$Data['id_dossier'])->setFrom([$Data['mailVendeur'] => 'Voyaneo retour Client']);
      });


    }
  }




  function sendMail($slugPage)
  {
    $dossier = \DB::table('dossiers')
      ->where('id', $slugPage)
      ->first();

    $client = \DB::table('clients')
      ->where('id', $dossier->id_client)
      ->first();

    $lienVendeur = \DB::table('lien_dossier_vendeur')
      ->where('id_dossier', $dossier->id)
      ->first();

    $vendeur = \DB::table('vendeurs')
      ->where('id', $lienVendeur->id_vendeur)
      ->first();

    $Data = [
      'id_dossier' => $dossier->id,
      'id_pkg' => $dossier->id_pkg,
      'client_mail' => $client->mail,
      'civilite' => $client->civilite,
      'client_nom' => $client->nom,
      'client_prenom' => $client->prenom,
      'date_confirm' => $dossier->date_confirm,
      'mailVendeur' => $vendeur->mail,
      'VendeurNom' => $vendeur->nom,
      'VendeurPrenom' => $vendeur->prenom,
      'to' => $dossier->to,
    ];

    var_dump($dossier->dest_hotel);

    \Mail::send('email.index2', $Data, function($message) use ($Data){
      $message->to($Data['client_mail'], $Data['civilite'].' '.$Data['client_nom'].' '.$Data['client_prenom'])->subject("Retour Client Voyaneo dossier N°".$Data['id_dossier'])->setFrom([$Data['mailVendeur'] => 'Voyaneo retour Client']);
    });

    \Mail::send('email.index2', $Data, function($message) use ($Data){
      $message->to('fabien@lead-factory.net', 'Fabien')->subject("Retour Client Voyaneo dossier N°".$Data['id_dossier'])->setFrom([$Data['mailVendeur'] => 'Voyaneo retour Client']);
    });

    \Mail::send('email.index2', $Data, function($message) use ($Data){
      $message->to('fabienp@sc2consulting.fr', 'Fabien')->subject("Retour Client Voyaneo dossier N°".$Data['id_dossier'])->setFrom([$Data['mailVendeur'] => 'Voyaneo retour Client']);
    });

//fabien.perreonsimenot@sfr.fr
    return view('email.index2', $Data);
  }




  function sendMailFactureConfirmationCGV ($dossier)
  {
      $doss = \DB::table("dossiers")->where('id', $dossier)->first();
      $client = \DB::table("clients")->where("id", $doss->id_client)->first();

      $objet = \Input::get("ObjetMail");
      $message = \Input::get("Message");
      $IsSold = \Input::get("IsSold");
      $FactureNS = \Input::get("factures_listeNS");
      $FactureAS = \Input::get("factures_listeAS");
      $data = array();

      \Mail::send([],[], function ($mail) use ($message,$dossier, $IsSold, $client, $objet, $FactureAS, $FactureNS){
         $mail->from('serviceclient@voyaneo.com', "Voyaneo");
         $mail->to($client->mail)->cc("serviceclient@voyaneo.com");
         $mail->subject($objet);
         $mail->setBody($message, 'text/html');



          $directory_facture = storage_path()."/app/factures/";
          $directory_confirmations = storage_path()."/app/confirmations/";
          $directory_cgv = storage_path()."/app/CGV/";
          $directory_uploaded_file = storage_path().'/app/dossiers/upload/'.$dossier."/";

          if($IsSold == null || $IsSold == 0) {
              $confirmations = scandir($directory_confirmations);
              $cgvs = scandir($directory_cgv);

              if (file_exists($directory_uploaded_file)) {
                  $raw_uploaded_file = scandir($directory_uploaded_file);
                  array_splice($raw_uploaded_file, 0, 2);
                  $uploaded_files = $raw_uploaded_file;
                  foreach ($uploaded_files as $file) {
                      $mail->attach($directory_uploaded_file . $file);
                  }
              } else {
                  $uploaded_files = null;
              }
              $confirmation = null;
              $cgv = $cgvs[2];
              $mail->attach($directory_cgv . $cgv);

              $mail->attach($directory_facture.$FactureNS);

              $index = 0;
              while ($index < sizeof($confirmations)) {
                  if (preg_match("/_" . $dossier . "_/", $confirmations[$index])) {
                      $confirmation = $confirmations[$index];
                      $mail->attach($directory_confirmations . $confirmation);
                      break;
                  }
                  $index++;
              }
          }
          else
          {
              $facture = null;
              $mail->attach($directory_facture.$FactureAS);
          }

      });


      return redirect("/dossiers");
  }


}
