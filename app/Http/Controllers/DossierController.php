<?php

namespace App\Http\Controllers;

use Storage;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;



class DossierController extends controller
{
    function showDossiers()
    {
        $dossiers = \DB::table("dossiers")->orderBy("dossiers.id", "desc")->get();
        $user = \Auth::user();

        return view("dossiers.show_dossiers")
            ->with("dossiers", $dossiers)
            ->with("user", $user);
    }

    function delete_file($dossier, $file)
    {
        Storage::delete("/dossiers/upload/".$dossier."/".$file);

        return back()
            ->with('success','You have successfully deleted file.');
    }

    function upload_file(Request $request, $dossier)
    {
        $this->validate($request, [
            'uploader_file'=>'required',
        ]);

        Storage::put("/dossiers/upload/".$dossier."/".$request->file("uploader_file")->getClientOriginalName(), file_get_contents($request->file("uploader_file")->getRealPath()));

        return back()
            ->with('success','You have successfully upload file.');
    }

    function sendingMailEditor($dossier)
    {
        $doss = \DB::table('dossiers')->where('id', $dossier)->first();
        $client = \DB::table("clients")->where("id", $doss->id_client)->first();
        $user = \Auth::user();

        $directory_facture = storage_path()."/app/factures";
        $directory_confirmations = storage_path()."/app/confirmations";
        $directory_cgv = storage_path()."/app/CGV";
        $directory_uploaded_file = storage_path().'/app/dossiers/upload/'.$dossier;

        $factures = scandir($directory_facture);
        $confirmations = scandir($directory_confirmations);
        $cgvs = scandir($directory_cgv);

        if(file_exists($directory_uploaded_file))
        {
            $raw_uploaded_file = scandir($directory_uploaded_file);
            array_splice($raw_uploaded_file, 0,2);
            $uploaded_files = $raw_uploaded_file;
        }
        else
        {
            $uploaded_files = null;
        }

        $facture = null;
        $confirmation = null;
        $cgv = $cgvs[2];

        $index = 0;
        $facturesDossiers = array();
        while($index < sizeof($factures))
        {
            if(preg_match("/_".$dossier."_/",$factures[$index]))
            {
                array_push($facturesDossiers, $factures[$index]);
            }
            $index++;
        }

        $index = 0;
        while($index < sizeof($confirmations))
        {
            if(preg_match("/_".$dossier."_/",$confirmations[$index]))
            {
                $confirmation = $confirmations[$index];
                break;
            }
            $index++;
        }

        return view("dossiers.sendingMailEditor")
            ->with('dossier', $doss)
            ->with('client', $client)
            ->with("factures", $facturesDossiers)
            ->with("confirmation", $confirmation)
            ->with("cgv", $cgv)
            ->with("uploaded_files", $uploaded_files)
            ->with("user", $user);
    }



}