<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserClient;

class Clients extends Model
{
  protected $table = 'clients';
  protected $fillable = ['id','civilité', 'prenom', 'nom','dateDeNaissance','adresse', 'numeroTelephone','email'];
}
