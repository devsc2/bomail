<?php

use Illuminate\Database\Eloquent\Model;

class lien_dossier_vendeur extends Model
{
  protected $table = 'lien_dossier_vendeur';
  protected $fillable = ['id_dossier', 'id_vendeur'];
}
