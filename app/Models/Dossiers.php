<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserClient;

class Dossiers extends Model
{
  protected $table = 'dossiers';
  protected $fillable = ['id', 'id_client', 'id_pkg','date_fin','dest_pays', 'date_confirm','to'];

}
