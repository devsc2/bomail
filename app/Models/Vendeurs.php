<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserClient;

class Vendeurs extends Model
{
  protected $table = 'vendeurs';
  protected $fillable = ['prenom', 'nom','rang','mail'];
}
