<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();
        });


        \DB::table('users')->delete();

        $now = date('Y-m-d H:i:s');
        DB::table('users')->insert([
            'name'              => 'Yves',
            'email'             => 'yveschaponic@gmail.com',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
        print_r("seed 2");
        DB::table('users')->insert([
            'name'              => 'Roxana',
            'email'             => 'roxana@voyaneo.com',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
        print_r("seed 3");
        DB::table('users')->insert([
            'name'              => 'Alexandra',
            'email'             => 'alexandra@voyaneo.com',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
        print_r("seed 4");
        DB::table('users')->insert([
            'name'              => 'Alexandre',
            'email'             => 'alexandre@voyaneo.com',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
        print_r("seed 5");
        DB::table('users')->insert([
            'name'              => 'Info',
            'email'             => 'info@voyaneo.com',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
        print_r("seed 6");
        DB::table('users')->insert([
            'name'              => 'Dev',
            'email'             => 'dev@sc2consulting.fr',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
