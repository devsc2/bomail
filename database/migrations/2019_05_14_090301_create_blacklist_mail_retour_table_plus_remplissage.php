<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlacklistMailRetourTablePlusRemplissage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blacklist_mail_retour', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_dossier');
            $table->string('mail_client');
            $table->timestamps();
        });

        \DB::table('blacklist_mail_retour')->insert(['id' => 1, 'id_dossier' => 2947, 'mail_client' => 'gilotin.robert@neuf.fr']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blacklist_mail_retour');
    }
}
