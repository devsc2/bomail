<?php

use App\User;
use Illuminate\Database\Seeder;

class users_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('users')->delete();

        $now = date('Y-m-d H:i:s');
        print_r("seed 1");
        DB::table('users')->insert([
            'name'              => 'Yves',
            'email'             => 'yveschaponic@gmail.com',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
        print_r("seed 2");
        DB::table('users')->insert([
            'name'              => 'Roxana',
            'email'             => 'roxana@voyaneo.com',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
        print_r("seed 3");
        DB::table('users')->insert([
            'name'              => 'Alexandra',
            'email'             => 'alexandra@voyaneo.com',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
        print_r("seed 4");
        DB::table('users')->insert([
            'name'              => 'Alexandre',
            'email'             => 'alexandre@voyaneo.com',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
        print_r("seed 5");
        DB::table('users')->insert([
            'name'              => 'Info',
            'email'             => 'info@voyaneo.com',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);
        print_r("seed 6");
        DB::table('users')->insert([
            'name'              => 'Dev',
            'email'             => 'dev@sc2consulting.fr',
            'password'          => Hash::make('aqwzsx'),
            'created_at'        => $now,
            'updated_at'        => $now,
        ]);

    }
}
