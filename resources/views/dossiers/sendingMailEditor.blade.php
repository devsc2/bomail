@extends('backOffice.bo_layout')

@section('content')
    <center>
        <h1 class="title">Envoyer un email au dossier numéro : {{$dossier->id}} au nom de {{$client->nom}}</h1>
    </center>
    <div style="margin-top: 3%;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="emailEditor" action="/send_email_{{$dossier->id}}" method="POST">
            {{ csrf_field() }}
            <center>
                <button type="submit" class="btn btn-success" style="font-size: 1.5em;">Envoyer !</button>
            </center>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <center>
                    <div class="form-group">
                        <label id="LabelForm" for="ObjetMail">Objet :</label><br/>
                        <input type="text" id="ObjetMail" class="ObjetMail" name="ObjetMail" value="Voyaneo - Votre facture et confirmation (dossier {{$dossier->id}})">
                    </div>
                    <div class="form-group">
                        <label id="LabelForm" for="Message">Message :</label><br/>
                        <textarea style="text-align: justify;" id="Message" name="Message" class="CorpsMessage">
                            Bonjour {{$client->civilite}} {{$client->nom}},<br/><br/>

Suite à votre réservation, je vous prie de trouver ci-joint votre facture, votre confirmation de voyage, ainsi que nos conditions générales de vente.<br/><br/>

Votre carnet de voyage vous sera envoyé par e-mail 72h avant le départ.<br/><br/>

Merci de nou faire parvenir <span style="text-decoration: underline"><strong>par retour de mail le plus rapidement possible copie ou scan des passeports</strong></span><br/><br/>
<span style="background-color: rgb(246,255,0);" data-mce-style="background-color:#f6ff00;">
    <strong>FORMALITÉS pour les ressortissants français:</strong> Passeport valable 6 mois après la date de retour + ESTA obligatoire avant départ  . Pour les autres nationalités, merci de vous rapprocher de votre consulat<br/><br/>
</span>
<span style="color: rgb(35,111,161);" data-mce-style="color:#236fa1;">
    ESTA à remplir sur le site</span> <a href="https://esta.cbp.dhs.gov/esta/" data-mce-href="https://esta.cbp.dhs.gov/esta/">https://esta.cbp.dhs.gov/esta/</a> <strong><span style="color:rgb(224,62,45);" data-mce-style="color:#e03e2d;">(le nom indiqué dans l'ESTA doit être identique à celui indiqué sur le billet d'avion – merci de faire l'ESTA au noms de jeune fille uniquement pour les femmes )</span></strong><br/><br/>

<span style="color:rgb(53,152,219);" data-mce-style="color:#3598db;">
Le formulaire ESTA doit être rempli au plus tard 72 heures avant le départ mais désormais les autorités américaines recommandent fortement de le faire dès que le voyage est planifié. Si la demande d'autorisation préalable apparaît EN ATTENTE sur l'écran, les voyageurs doivent se reconnecter au site officiel de l'ESTA au minimum 48 heures plus tard pour en connaître le statut. Attention, la confirmation ou le refus de l'ESTA n'est jamais adressé par mail, seule une connexion au site officiel permet d'obtenir une réponse : <a href="https://esta.cbp.dhs.gov/esta/" data-mce-href="https://esta.cbp.dhs.gov/esta/">https://esta.cbp.dhs.gov/esta/</a> le formulaire est strictement personnel et sous la responsabilité du passager <br/><br/>
</span>
<span style="color: rgb(224, 62, 45);text-align: justify" data-mce-style="color: #e03e2d;">Voyaneo ne s'occupe jamais de visa, et décline toute responsabilité pour non respect des règles d'admission sur le territoire du pays visité. Pour plus d'informations, merci de vous renseigner sur le site <a href="http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/">http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/</a> en choisissant le/les pays concernés ! Bien évidemment nous restons  à votre disposition si besoin</span><br/><br/>

Pour plus d'informations sur les formalités administratives et de santé (visas, vaccins etc) , je vous invite à consulter la rubrique entrée/séjour du site du Ministère des Affaires Étrangères : <a href="https://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays-destination/etats-unis/#entree">https://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays-destination/etats-unis/#entree</a><br/><br/>

Je reste bien évidemment à votre disposition pour toute demande éventuelle, et je vous souhaite une excellente journée<br/><br/>

Bien à vous,<br/><br/>
--<br/>
Roxana - Directrice Agence<br/>
Agencedevoyage.com devient Voyaneo.com !<br/>
Tél : 01 77 69 02 57<br/>
e-mail : roxana@voyaneo.com
                        </textarea>
                    </div>
                </center>
            </div>
            <div style="float:right;right:0;position:fixed;" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div id="Notsold" style="display:block;border:solid 3px #f49401; border-radius: 10px;height: 350px;">
                    <center><h3>Pièces jointes</h3></center>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 75%;">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                            <center><h5>Facture :</h5></center>
                            <center>@if($factures == null)VIDE
                                    @else
                                    <select name="factures_listeNS" form="emailEditor">
                                        @foreach($factures as $facture)
                                            <option value="{{$facture}}">{{$facture}}</option>
                                        @endforeach
                                    </select>
                                    @endif
                            </center>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <center><h5>Confirmation :</h5></center>
                            <center>@if($confirmation == null)VIDE @else{{$confirmation}}@endif</center>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <center><h5>CGV :</h5></center>
                            <center>{{$cgv}}</center>
                        </div>
                        <div style="margin-top:5%;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <center><h5>Pièces Importées :</h5></center>
                            @if($uploaded_files != null)
                            {{$uploaded_files[0]}} <a href="/delete_file_{{$dossier->id}}_{{$uploaded_files[0]}}"><i class="fa fa-times-circle" style="color:darkred"></i></a>
                            @for($i = 1; $i < sizeof($uploaded_files); $i++)
                                | {{$uploaded_files[$i]}}  <a href="/delete_file_{{$dossier->id}}_{{$uploaded_files[$i]}}"><i class="fa fa-times-circle" style="color:darkred"></i></a>
                            @endfor
                            @endif
                        </div>
                    </div>
                    <center><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#upload_file_modal">Ajouter un fichier</button></center>
                </div>
                <div id="Sold" style="display:none;border:solid 3px #f49401; border-radius: 10px;height: 350px;">
                    <center><h3>Pièces jointes</h3></center>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 75%;">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <center><h5>Facture :</h5></center>
                            <center>@if($factures == null)VIDE
                                    @else
                                        <select name="factures_listeAS" form="emailEditor">
                                            @foreach($factures as $facture)
                                                <option value="{{$facture}}">{{$facture}}</option>
                                            @endforeach
                                        </select>
                                @endif</center>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 3%;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <center><input type="checkbox" class="IsSold" name="IsSold" id="IsSold" onchange="IsSolded(this)"/> <h5>Facture soldée</h5></center>
                </div>
                <div style="margin-top:3%;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <center>
                        <h3>Destinataire : {{$client->mail}}</h3>
                    </center>
                </div>
                <div style="margin-top:3%;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                </div>
            </div>
        </form>
    </div>

    <div id="upload_file_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="upload_file_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Uploader un fichier</h5><button class="btn btn-danger" data-dismiss="modal" aria-label="Close" style="float: right;">x</button>
                </div>
                <div class="modal-body">
                    <form action="/upload_file_{{$dossier->id}}" method="post" id="uploader" name="uploader" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="file" id="uploader_file" name="uploader_file">
                        <button type="submit" id="uploader_sub" name="uploader">Uploader</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
    $Today = date("Y-m-d");

    $result = strtotime($dossier->date_deb) - strtotime($Today);
    $days = intval($result/86400);
?>
    <script>
        tinymce.init({
            selector: '#Message'
        });
        $('#modal-categorie').on('show.bs.modal', function (event) {
            //var button = $(event.relatedTarget);
        });

        function IsSolded(obj)
        {
            console.log(obj.value);
            if(obj.value != null && (obj.value == "on" || obj.value==0))
            {
                obj.value=1;
                let newMessage = "Bonjour {{$client->civilite}} {{$client->nom}}<br/><br/> Étant à {{$days}} jours de votre départ, nous avons procédé au solde de votre<br/> dossier, correspondant à la somme de PRIX€ sur la carte ayant servi à<br/> la réservation.<br/><br/> Vous trouverez en pièce jointe une facture acquittée.<br/> <br/> Bien à vous,<br/> --<br/> Roxana - Directrice Agence<br/> Agencedevoyage.com devient Voyaneo.com !<br/> Tél : 01 77 69 02 57<br/> e-mail : roxana@voyaneo.com<br/>";
                let newSubject = "Voyaneo - Votre facture mise a jour (dossier {{$dossier->id}})";
                console.log("ici");
                document.getElementById("Message").innerHTML = newMessage;
                document.getElementById("Message").value = newMessage;
                document.getElementById("Message").innerText = newMessage;
                tinymce.get('Message').setContent(newMessage);

                document.getElementById("ObjetMail").value = newSubject;
                document.getElementById("ObjetMail").innerHTML = newSubject;

                document.getElementById("Notsold").style.display = "none";
                document.getElementById("Sold").style.display = "block";
            }
            else
            {
                obj.value = 0;
                let message= "Bonjour {{$client->civilite}} {{$client->nom}},<br/><br/>Suite à votre réservation, je vous prie de trouver ci-joint votre facture, votre confirmation de voyage, ainsi que nos conditions générales de vente.<br/><br/>Votre carnet de voyage vous sera envoyé par e-mail 72h avant le départ.<br/><br/>Merci de nou faire parvenir <span style=\"text-decoration: underline\"><strong>par retour de mail le plus rapidement possible copie ou scan des passeports</strong></span><br/><br/><span style=\"background-color: rgb(246,255,0);\" data-mce-style=\"background-color:#f6ff00;\"><strong>FORMALITÉS pour les ressortissants français:</strong> Passeport valable 6 mois après la date de retour + ESTA obligatoire avant départ  . Pour les autres nationalités, merci de vous rapprocher de votre consulat<br/><br/></span><span style=\"color: rgb(35,111,161);\" data-mce-style=\"color:#236fa1;\">ESTA à remplir sur le site</span> <a href=\"https://esta.cbp.dhs.gov/esta/\" data-mce-href=\"https://esta.cbp.dhs.gov/esta/\">https://esta.cbp.dhs.gov/esta/</a> <strong><span style=\"color:rgb(224,62,45);\" data-mce-style=\"color:#e03e2d;\">(le nom indiqué dans l'ESTA doit être identique à celui indiqué sur le billet d'avion – merci de faire l'ESTA au noms de jeune fille uniquement pour les femmes )</span></strong><br/><br/><span style=\"color:rgb(53,152,219);\" data-mce-style=\"color:#3598db;\">Le formulaire ESTA doit être rempli au plus tard 72 heures avant le départ mais désormais les autorités américaines recommandent fortement de le faire dès que le voyage est planifié. Si la demande d'autorisation préalable apparaît EN ATTENTE sur l'écran, les voyageurs doivent se reconnecter au site officiel de l'ESTA au minimum 48 heures plus tard pour en connaître le statut. Attention, la confirmation ou le refus de l'ESTA n'est jamais adressé par mail, seule une connexion au site officiel permet d'obtenir une réponse : <a href=\"https://esta.cbp.dhs.gov/esta/\" data-mce-href=\"https://esta.cbp.dhs.gov/esta/\">https://esta.cbp.dhs.gov/esta/</a> le formulaire est strictement personnel et sous la responsabilité du passager <br/><br/></span><span style=\"color: rgb(224, 62, 45);text-align: justify\" data-mce-style=\"color: #e03e2d;\">Voyaneo ne s'occupe jamais de visa, et décline toute responsabilité pour non respect des règles d'admission sur le territoire du pays visité. Pour plus d'informations, merci de vous renseigner sur le site <a href=\"http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/\">http://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/</a> en choisissant le/les pays concernés ! Bien évidemment nous restons  à votre disposition si besoin</span><br/><br/>Pour plus d'informations sur les formalités administratives et de santé (visas, vaccins etc) , je vous invite à consulter la rubrique entrée/séjour du site du Ministère des Affaires Étrangères : <a href=\"https://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays-destination/etats-unis/#entree\">https://www.diplomatie.gouv.fr/fr/conseils-aux-voyageurs/conseils-par-pays-destination/etats-unis/#entree</a><br/><br/>Je reste bien évidemment à votre disposition pour toute demande éventuelle, et je vous souhaite une excellente journée<br/><br/>Bien à vous,<br/><br/>--<br/>Roxana - Directrice Agence<br/>Agencedevoyage.com devient Voyaneo.com !<br/>Tél : 01 77 69 02 57<br/>e-mail : roxana@voyaneo.com";
                let subject = "Voyaneo - Votre facture et confirmation (dossier {{$dossier->id}})";
                document.getElementById("Message").innerHTML = message;
                document.getElementById("Message").value = message;
                document.getElementById("Message").innerText = message;
                tinymce.get('Message').setContent(message);

                document.getElementById("ObjetMail").value = subject;
                document.getElementById("ObjetMail").innerHTML = subject;

                document.getElementById("Notsold").style.display = "block";
                document.getElementById("Sold").style.display = "none";
            }
        }
    </script>
@endsection
