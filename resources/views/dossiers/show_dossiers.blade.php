@extends('backOffice.bo_layout')

@section('content')
    <center>
        <h1 class="title">Liste des dossiers</h1>
    </center>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="travelo-box mainGuide">
        <div class="table-responsive">
            <div>
                <table id="tableProduct" class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th style="text-align:center;cursor: pointer;">ID</th>
                        <th style="text-align:center;cursor: pointer;">ID PKG</th>
                        <th style="text-align:center;cursor: pointer;">Client</th>
                        <th style="text-align:center;cursor: pointer;">Date Départ</th>
                        <th style="text-align:center;cursor: pointer;">Date Retour</th>
                        <th style="text-align:center;cursor: pointer;">Facture</th>
                        <th style="text-align:center;cursor: pointer;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($dossiers as $d)
                        <?php
                            $client= \DB::table('clients')->where('id', $d->id_client)->first();

                        $directory_facture = storage_path()."/app/factures/";
                        $factures = scandir($directory_facture);
                        $index = 0;
                        $facturesDossiers = array();
                        while($index < sizeof($factures))
                        {
                            if(preg_match("/_".$d->id."_/",$factures[$index]))
                            {
                                array_push($facturesDossiers, $factures[$index]);
                            }
                            $index++;
                        }
                            ?>
                            <tr style="cursor: pointer;">
                                <td style="text-align:center;">{{ $d->id }}</td>
                                <td style="text-align:center;">{{ $d->id_pkg }}</td>
                                <td style="text-align:center;">{{ $client->nom }}</td>
                                <td style="text-align:center;">{{ $d->date_deb }}</td>
                                <td style="text-align:center;">{{ $d->date_fin }}</td>
                                <td style="text-align:center;">
                                    <select class="factSelector" id="factures_{{$d->id}}" name="facture_{{$d->id}}" data-position="{{$d->id}}" onchange="selectNewFacture(this)">
                                        <option>Toutes les Factures:</option>
                                        @foreach($facturesDossiers as $facture)
                                            <option value="{{$facture}}" data-position="{{$d->id}}" >{{$facture}}</option>
                                        @endforeach
                                    </select>
                                    <a id="PrintFacture_{{$d->id}}" href="/factures/" target="_blank"><button type="button" class="btn btn-success">Imprimer Facture</button></a>
                                </td>
                                <td style="text-align:center;">
                                    <a href="/sendingmaileditor/{{$d->id}}"><button type="button" class="btn btn-warning">Envoyer Facture + Confirmation</button></a>
                                </td>
                            </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>


        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <style src="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
            .dataTables_paginate a, .dataTables_paginate span{
                margin-right: 5px;
            }
        </style>

        <script>

            function selectNewFacture(obj)
            {
                let position = obj.dataset.position;
                console.log(position);
                let button = document.getElementById("PrintFacture_"+position);
                let facture = obj.options[obj.selectedIndex].text;

                button.href = "/factures/"+facture;
            }

            $('#modal-categorie').on('show.bs.modal', function (event) {
                //var button = $(event.relatedTarget);
            });
            $(document).ready(function () {
                $("#tableProduct").DataTable();
            });
        </script>
@endsection