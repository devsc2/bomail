<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!--><html lang="fr"><!--<![endif]-->
    <head>
        <title>BackOffice Envoie de Mail</title>
        <meta charset="utf-8">
        <link rel="icon" href="images/icon/favicon.ico" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen" title="no title" charset="utf-8">

        <script src="https://cdn.ckeditor.com/ckeditor5/15.0.0/classic/ckeditor.js"></script>
        <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

        <script src="{{elixir('js/base.min.js')}}"></script>
        <script src="{{elixir('js/plugin.min.js')}}"></script>

        <link href="{{elixir('css/plugin.min.css')}}" media="screen" rel="stylesheet" type="text/css" />
        <link href="{{elixir('css/theme.min.css')}}" media="screen" rel="stylesheet" type="text/css" />
        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <!-- jQuery UI -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex,nofollow"/>
    </head>


    <body>
        <div style="padding: 0;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="header" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="logo" class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <a href="/"><img src="/images/logo.png"> </a>
                </div>
                <div id="navbar" class="col-lg-6 col-md-6 col-sm-3 col-xs-3">
                    <ul class="menu" id="menu">
                        <a href="/dossiers"><li>Dossiers</li></a>
                        <a href=""><li>MENU A AJOUTER</li></a>
                    </ul>
                </div>
                <div id="user" class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <h4>{{$user->email}}</h4>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @yield('content')
            </div>
        </div>
    </body>
</html>