<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
  <style>
    img{
    	width: 100%;
    }
    table {
    	table-layout: fixed;
    }
    @media screen and (max-width:480px) {
    	table {
    		width: 100%!important;
    	}
    }
  </style>
  <body>
  	<table border='0' cellpadding='0' cellspacing='0' height='100%' width='100%'>
  		<tr>
  			<td align='center' valign='top'>
  				<table border='0' cellpadding='20' cellspacing='0' width='600' style='table-layout:fixed;'>
  					<tr>
  						<td align='center'>
  							<a href='http://www.voyaneo.com/' alt='page accueil'><img style='wodth:100%;' alt='Logo' src='http://www.voyaneo.com/build/images/logo/logo.png' /></a>
  						</td>
  					</tr>
  					<tr>
  						<td align='center'>
  							<p style='font-family: verdana;font-size: 13px;text-align: justify;width: 505px;'><b style='color:#f16915'>Bonjour, @if($civilite != ''){{$civilite}}@endif {{$client_nom}}</b><br/><br/>
  							Votre voyage vient de prendre fin... Nous espérons que vous avez passé d'excellentes vacances et que votre séjour s'est déroulé dans les meilleures conditions.
  							</p>
  						</td>
  					</tr>
  					<tr style='display:block;'>
  						<td width='30%'>
  							<a href='http://www.voyaneo.com/' alt='Page accuei'><img alt='Avis Vérifiés' src='http://cl.avis-verifies.com/fr/widget4/iframe/logo_170.png' width='140' /></a>
  						</td>
  						<td width='70%' >
  							<p style='font-family: verdana;font-size: 13px;'>
  								Nous aimerions connaître votre avis au sujet de votre réservation sur <a href='http://voyaneo.com/avisverifies' style='color: #f16915' >voyaneo.com</a>. <br>
  								 Vos notes et commentaires aideront notre site à améliorer ses services. <br>
  								<strong>Votre expérience est précieuse, partagez-la !</strong><br>
  								Cela ne vous prendra que quelques secondes.</p>
  							<p style='text-align:center;'>
  								<a href='http://voyaneo.com/avisverifies?to_reference={{$id_pkg}}&to_name={{$to}}&order_reference={{$id_dossier}}&client_mail={{$client_mail}}&client_nom={{$client_nom}}&client_prenom={{$client_prenom}}&order_date={{$date_confirm}}' style='color: #f16915; font-size: 20px;'>&rarr; DONNER MON AVIS &larr;</a>
  							</p>
  						</td>
  					</tr>
  					<tr>
  						<td>
  							<p style='font-family: verdana;font-size: 13px;width: 505px;'>
  								N'hésitez pas à également à partager vos impressions, commentaires, souvenirs et vos plus belles photos sur notre page <a href='https://www.facebook.com/voyaneo/' style='color: #f16915'>Facebook</a>
  							</p>
  							<p>
  							<i>A très bientôt sur <a href='http://www.voyaneo.com/' alt=''>voyaneo.com</a></i><br><br>
  							Cordialement, <br><br>
  				    		L'équipe voyaneo.com<br/>
  							Tél : 01 77 69 02 50<br/>
  							info@voyaneo.com
  							</p>
  						</td>
  					</tr>
  				</table>
  			</td>
  		</tr>
  	</table>
  </body>
</html>
